
// this function will patch a click method onto anchor elements for Mozilla. Due apparently to confusion in the early DOM spec, anchor elements in mozilla are missing click methods. This will attach a click method to HTMLElement so that all elements, being descendants of this object will inherit it if they dont have one of their own. 
if (!document.all) {
  try {
		document.createElement('a');
		HTMLElement.prototype.click = function () {
			if (typeof this.onclick == 'function') {
				if (this.onclick({type: 'click'}) && this.href) 
					window.open(this.href, this.target ? this.target : '_self');
			}
			else if (this.href)
				window.open(this.href, this.target ? this.target : '_self');
		}
	}
	catch (e) {
		window.status='Warning: your browser is unable to attach click methods to all elements.';
	}
}


// only for use with absolute positioned elements. 

function mouseTracker(e) {
	mouseX = (document.all) ? window.event.x : e.pageX;
	mouseY = (document.all) ? window.event.y : e.pageY;
}

window.onmousemove = mouseTracker;
var mouseX = 0;
var mouseY = 0;


function globalComponentInit() {
	if (typeof(ToolbarManager)!="undefined") {
		ourToolbarManager = new ToolbarManager();
		ourToolbarManager.init();
	}
	if (typeof(ButtonManager)!="undefined") {
		ourButtonManager = new ButtonManager();
		ourButtonManager.init();
	}
	if (typeof(TransferBoxManager)!="undefined") {
		ourTransferBoxManager = new TransferBoxManager();
		ourTransferBoxManager.init();
	}
	if (typeof(DropdownMenuManager)!="undefined") {
		ourDropdownMenuManager = new DropdownMenuManager();
		ourDropdownMenuManager.init();
		document.onmousedown= dropdownMenuManager_clearMenus;
	}
	if (typeof(NavigationControlManager)!="undefined") {
		ourNavigationControlManager = new NavigationControlManager();
		ourNavigationControlManager.init();
	}
	if (typeof(TabManager)!="undefined") {
		ourTabManager = new TabManager();
		ourTabManager.init();
	}
	if (typeof(TableManager)!="undefined") {
		ourTableManager = new TableManager();
		ourTableManager.init();
	
		
		tableManager_windowResize();
		// to disable the default behaviour that ctrl-clicks selects a whole word. 
		// Ctrl-click within hp applications has a special meaning. 
		if (document.all) {					
			document.onselectstart = function() {var ctrlKey = (document.all) ? window.event.ctrlKey :mozEvent.ctrlKey;if (ctrlKey)return false;}
		}
	}
	if (typeof(TreeManager)!="undefined") {
		ourTreeManager = new TreeManager();
		ourTreeManager.init();
	}
	if (typeof(TreeTableManager)!="undefined") {
		ourTreeTableManager = new TreeTableManager();
		ourTreeTableManager.init();
	}	
}
var ourToolbarManager = null;
var ourButtonManager = null;
var ourTransferBoxManager = null;
var ourDropdownMenuManager = null;
var ourNavigationControlManager = null;
var ourTabManager = null;
var ourTableManager = null;
var ourTreeManager = null;
var ourTreeTableManager = null;


// a little global function that returns a reasonably cross-browser reference to the originating object of the given mozEvent. 
function getEventOriginator(mozEvent) {
	return (document.all) ?  event.srcElement : mozEvent.target;
}

// attaches event handlers that various components need, without destroying the application's own event handlers in the process.
function reconcileEventHandlers() {
	if (window.onload) {   // some onload code has been placed into the body tag, or some function assigned to window.onload, before this function was called.
		// capture it as a function object, assign it to a var. 
		var applicationLevelOnload = window.onload;  		
		// execute our globalComponentInit, then execute the function object holding the other onload code. 
		eval("window.onload = function() {globalComponentInit();var applicationLevelOnload="+applicationLevelOnload+";applicationLevelOnload()}");
	}
	else { // window.onload is still untouched when this function gets called. 
		window.onload = globalComponentInit;
	}

	if (typeof(TableManager)!="undefined") {
		
		if (window.onresize) {
			var applicationLevelOnresize = window.onresize;
			eval("window.onresize = function() {tableManager_windowResize();var applicationLevelOnresize="+applicationLevelOnresize+";applicationLevelOnresize();}");
		}
		else { // window.onload is still untouched when this function gets called. 
			window.onresize = tableManager_windowResize;
		}
	}
}
// some implementations where classNames need to be changed may have multiple class selectors in their classname.  This will add a classname as a class selector, if it is not already present, and without deleting other unrelated classnames that might be present. 
function appendClassName(obj, newClassName) {
	if (obj.className.indexOf(newClassName)!=-1){
		return true;
	}
	if (!obj.className){
		obj.className = newClassName;
	} else {
		obj.className = obj.className + " " + newClassName;
	}
}
function removeClassName(obj, classNameToRemove) {
	var newClassName = obj.className.replace(" "+classNameToRemove,"");
	newClassName = newClassName.replace(classNameToRemove+" ","");
	if (obj.className.length == newClassName.length) {
		newClassName = newClassName.replace(classNameToRemove,"");
	}
	obj.className = newClassName;
}


//tooltip
var offsetxpoint = 10; //Customize x offset of tooltip
var offsetypoint = 20; //Customize y offset of tooltip
var ie=document.all;
var ns6=document.getElementById && !document.all;
var enabletip=false;

function ietruebody(){
	if (document.documentElement && document.documentElement.clientHeight){
	// Explorer 6 Strict Mode{
		return document.documentElement;
	} else if (document.body){ // other Explorers
		return document.body;
	}


	return document.documentElement;
}

function showTip(thetext, thecolor, thewidth){
	var elm = document.getElementById("tooltip");
	if (typeof thewidth!="undefined") {
		elm.style.width=thewidth+"px";
	}
	if (typeof thecolor!="undefined" && thecolor!=""){
		elm.style.backgroundColor=thecolor;
	}
	elm.innerHTML=thetext;
	enabletip=true;
	return false;
}

function positiontip(e){
	var elm = document.getElementById("tooltip");
	if (enabletip){
		var curX=(ns6)?e.pageX : event.x+ietruebody().scrollLeft;
		var curY=(ns6)?e.pageY : event.y+ietruebody().scrollTop;
		
		var rightedge=ie? ietruebody().clientWidth-event.clientX-offsetxpoint : window.innerWidth-e.clientX-offsetxpoint-20;
		var bottomedge=ie? ietruebody().clientHeight-event.clientY-offsetypoint : window.innerHeight-e.clientY-offsetypoint-20;

		var leftedge=(offsetxpoint<0)? offsetxpoint*(-1) : -1000

		if (rightedge<elm.offsetWidth){
			elm.style.left=ie? ietruebody().scrollLeft+event.clientX-elm.offsetWidth+"px" : window.pageXOffset+e.clientX-elm.offsetWidth+"px";
		} else if (curX<leftedge){
			elm.style.left="-5px";
		} else {
			elm.style.left=curX+offsetxpoint+"px";
		}

		if (bottomedge<elm.offsetHeight){
			elm.style.top=ie? ietruebody().scrollTop+event.clientY-elm.offsetHeight-offsetypoint+"px" : window.pageYOffset+e.clientY-elm.offsetHeight-offsetypoint+"px";
		}else{
			elm.style.top=curY+offsetypoint+"px";
			elm.style.visibility="visible";
		}
	}
}

function hideTip(){
	var elm = document.getElementById("tooltip");
	enabletip=false;
	elm.style.visibility="hidden";
	elm.style.left="-1000px";
	elm.style.backgroundColor="";
	elm.style.width="";
}

document.onmousemove = positiontip;

//function used to write the time, at which the page is last refreshed, to the banner
function clock()
{
  var digital = new Date();
  var weekday = new Array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");
  var hours = digital.getHours();
  var minutes = digital.getMinutes();
  var seconds = digital.getSeconds();
  var date = digital.getDate();
  var month = digital.getMonth()+1;
  var year = digital.getFullYear();

  /* add zero paddings to time and date */
  hours = ((hours < 10) ? "0" : "") + hours;
  minutes = ((minutes < 10) ? "0" : "") + minutes;
  seconds = ((seconds < 10) ? "0" : "") + seconds;
  date = ((date < 10) ? "0" : "") + date;
  month = ((month < 10) ? "0" : "") + month;
  
  dispTime = weekday[digital.getDay()] + ", " + year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;

  return dispTime;
}

//function to open a new page in a new window with fixed width and height of 800x600
function openNewPage(url,urlname)
{
  WindowObjectReference = window.open(url,urlname,"width=800,height=600,resizable=yes,scrollbars=yes,toolbar=yes,location=yes,menubar=yes,status=1");
}

/* gas gauges */

//function to return div tag according to current ink level by controlling amt of gray background
function formatInkLevel(inkLevel)
{
   var tag;

   tag = '<div style="font-size:0px;height:';

   if(inkLevel == 0)
   {
      
      //even if current ink level is zero, we still want to show a little bit of the color for identification purpose
      tag = tag + '99';
   }
   else if(inkLevel > 100)
   {
      //set inkLevel to 100 if the value passed in is more than 100%
      tag = 100;
   }
   else
   {
      //gray background height is the inverse of ink level
      tag = tag + (100 - inkLevel);
   }

   tag = tag + 'px;width=100%;background-color:#eeeeee;"></div>';

   return tag;
}

//function that displays the ink status icons
function displayInkStatusImage(url)
{
   var tag;

   tag = '<img src="' + url + '" width="15" height="15" />' ;

   return tag;
}



/* Taken from tableManager.js */



function TableManager() {
	// a global var holding a roster of all scrollableTables onscreen. Used in windowResize, for the following reason.  If a tableis scrolled horizontally, and the window is widened, you basically need this to realign the columns as the table is sort of unscrollbarring itself.  onscroll on the div will not fire for us, although you might think it should. 
	this.scrollableTableIds = new Array();
}


TableManager.prototype.init = function() {
	tableNodeCollection = window.document.getElementsByTagName("table");

	// dealing with scrollingTables
	for (var i=0; i<tableNodeCollection.length; i++ ) {
		if (tableNodeCollection[i].className.indexOf("scrollingTable")!=-1) {
			var tableId = tableNodeCollection[i].getAttribute('id');
			// file it away for use by the windowResize function
			this.scrollableTableIds[this.scrollableTableIds.length] = tableId;
			// this is the big function that aligns the divs with the table columns
			tableNodeCollection[i].initialWidth = tableNodeCollection[i].offsetWidth;
			this.resizeTableColumns(tableId);
			// a nice way to leverage anonymous functions to allow us to keep event handler code out of the html. A nice side effect is that event handlers arent in place until the page is loaded, at which point they generally also become safe to run.
			eval("document.getElementById(tableId + '_scrollingTableDiv').onscroll=function() {ourTableManager.slideColumns('"+tableId+"')}");
		}
	}

	// dealing with tables that have 'select all' checkboxes.
	inputNodeCollection = window.document.getElementsByTagName("input");
	var chk;
	for (var i=0; i<inputNodeCollection.length; i++ ) {
		if (inputNodeCollection[i].getAttribute('tableid')) {
			chk = inputNodeCollection[i];
			var tableId = chk.getAttribute("tableid");
			if (tableId) {
				eval("chk.onclick = function(mozEvent) {ourTableManager.tableCheckboxToggleAll(mozEvent,this,'"+ tableId +"');};");
			}
			else alert('error: table has checkbox but checkbox has no tableid');
		}
		if (inputNodeCollection[i].getAttribute('rowselector')=="yes") { 

			//  if this is loading from a refresh rather than a load, or if an application has been implemented too quickly, radio or checkbox state might be different from the classnames as they came down from the server. 
			if (((inputNodeCollection[i].checked) || (inputNodeCollection[i].value=="1")) && (inputNodeCollection[i].parentNode.parentNode.className.indexOf("rowHighlight")==-1)) {
				appendClassName(inputNodeCollection[i].parentNode.parentNode, "rowHighlight");
			}
			else if (((!inputNodeCollection[i].checked) && (inputNodeCollection[i].value!="1")) && (inputNodeCollection[i].parentNode.parentNode.className.indexOf("rowHighlight")!=-1)) {
				removeClassName(inputNodeCollection[i].parentNode.parentNode,"rowHighlight");
				
			}


			// both of these result in the same data going into tableRowHighlightToggle, although the event will have different srcElement/currentTarget.  
			inputNodeCollection[i].onclick = function(mozEvent) {ourTableManager.tableRowHighlightToggle(mozEvent,this.parentNode.parentNode,true);};

			var checkingForPropertyViewTable = inputNodeCollection[i].parentNode;
			var isPropertyViewTable = false;
			while (checkingForPropertyViewTable.tagName!="TABLE") {
				checkingForPropertyViewTable = checkingForPropertyViewTable.parentNode;
			}
			if (checkingForPropertyViewTable.className.indexOf("propertyViewTable")!=-1)  isPropertyViewTable = true;
			
			inputNodeCollection[i].parentNode.parentNode.onclick = function(mozEvent) {return ourTableManager.tableRowHighlightToggle(mozEvent,this,true);};
			if (isPropertyViewTable) {
				
				if (!document.all) {	
					// an elegant way to disable ctrl-click on these elements for Mozilla browsers. 
					// IE unfortunately has to use less subtle methods, disabling all ctrl-selection by setting document.onselectstart to return false if CTRL key is pressed. This occurs in global.js if the TableManager is loaded. 
					inputNodeCollection[i].parentNode.parentNode.onmousedown = function(mozEvent) {return false;};
				}
			}
		}
	}
}

// Although in most cases of window resizing it's not necessary to recalculate column positions, if a table is partially scrolled horizontally, resizing the window can effectively scroll the div, and unfortunately in IE6 this does not trigger onscroll, so this amounts to a patch.
TableManager.prototype.windowResize = function() {
	for (var i=0; i<this.scrollableTableIds.length; i++) {
		this.slideColumns(this.scrollableTableIds[i]);
	}
}
// triggered by onclick on the master checkbox itself.  
TableManager.prototype.tableCheckboxToggleAll = function(mozEvent,masterCheckbox,uniqueTableId) {
	var tableNode = document.getElementById(uniqueTableId);
	
	
	inputNodeCollection = tableNode.getElementsByTagName("input");
	
	for (var i=0; i<inputNodeCollection.length; i++ ) {
		// only want checkboxes, not textfields and radios
		if (inputNodeCollection[i].getAttribute('type') == "checkbox")  {
			// careful not to toggle the masterCheckbox itself. 
			if (masterCheckbox != inputNodeCollection[i]) {
				// the checkbox is a rowSelector and not just some editable property checkbox.
				if (inputNodeCollection[i].getAttribute("rowselector")=="yes") {
					if (masterCheckbox.checked != inputNodeCollection[i].checked) {
						this.tableRowHighlightToggle(mozEvent,inputNodeCollection[i].parentNode.parentNode, false);
					}
				}
			}
		}
	}
}
// called by onclick on the table row.  This function will only be attached as an onclick handler to a row if either it contains a rowselector form element, or if the parent table is of class "propertyViewTable" 
TableManager.prototype.tableRowHighlightToggle = function(mozEvent,rowElement,checkOrigination) {
	var eventSource = getEventOriginator(mozEvent);

	// a list of various elements within tree rows upon which clicks should not toggle highlight state. 
	if ((eventSource.tagName=="IMG") ||
		(eventSource.tagName=="A") || ((eventSource.tagName=="DIV") && (eventSource.className=="treeControl"))) {
		return false;		
	}

	// the table node is used by the radio elements, which need to circle back onclick and change highlighting on other rows.
	// the other place it is used is in the Property View table, where presence of that "propertyViewTable" classname on the table must change the clicking interaction. 
	var tableNode = rowElement.parentNode;
	while (tableNode.tagName!= "TABLE") { tableNode = tableNode.parentNode }
	
	var isPropertyViewTable = (tableNode.className.indexOf("propertyViewTable")!=-1);


	// we will deal with state changes to the rowselector form elements.  (not highlighting, which is dealt with later in this function )
	inputNodeCollection = rowElement.getElementsByTagName("input");
	
	// obtaining a reference to the rowSelector element within the row.
	var ourRowSelectorFormElement;
	for (var i=0; i<inputNodeCollection.length; i++ ) {
		if (inputNodeCollection[i].getAttribute("rowselector")=="yes") {
			ourRowSelectorFormElement = inputNodeCollection[i];
		}
	}
	// only deal with the form element change if the click is not coming from the formElement itself. (in which case it will have taken care of its own state)
	if ((eventSource.tagName!="INPUT") || ( ( eventSource.tagName=="INPUT") && (eventSource.getAttribute("tableid")))) {
		// deals with cases where the click came from the row
		
		if (ourRowSelectorFormElement.getAttribute("type") == "checkbox") {
			// manually flip the checkbox. 
			ourRowSelectorFormElement.checked = !ourRowSelectorFormElement.checked;
		}
		if (ourRowSelectorFormElement.getAttribute("type") == "radio") {
			// manually flip the radio. 
			ourRowSelectorFormElement.checked = !ourRowSelectorFormElement.checked;
		}
		else if (ourRowSelectorFormElement.getAttribute("type") == "hidden") {
			if (ourRowSelectorFormElement.value == "1") ourRowSelectorFormElement.value=0;
			else ourRowSelectorFormElement.value = "1";
		}
	}



	// Dealing with the highlighting changes. 
	// class "propertyViewTable" tables have a significantly different interaction. 
	if (isPropertyViewTable) {
		var ctrlKey = (document.all) ? window.event.ctrlKey :mozEvent.ctrlKey;
		if (!ctrlKey) {
			var inputNodeCollection = tableNode.getElementsByTagName("INPUT");

			// go through all the inputs
			for (var j=0;j<inputNodeCollection.length;j++) {

				// only interested in rowselector, type="hidden" inputs. 
				if ((inputNodeCollection[j].getAttribute("type")=="hidden") && (inputNodeCollection[j].getAttribute("rowselector")=="yes")) {

					var tableRowNode = inputNodeCollection[j].parentNode;
					while (tableRowNode.tagName!= "TR") { tableRowNode = tableRowNode.parentNode; }
					
					// since CTRL is not pressed, we remove highlighting from all rows except the one clicked on.
					if (inputNodeCollection[j]!=ourRowSelectorFormElement)  {
						removeClassName(tableRowNode,"rowHighlight");
						inputNodeCollection[j].value = "0";
					}
					else {
						appendClassName(tableRowNode, "rowHighlight");	
					}
				}
			}
		}
		// Ctrl Key is pressed, which now allows multiple selection. In this case we only change highlighting for the row the user has clicked on.
		else {
			if (rowElement.className.indexOf("rowHighlight")!=-1) {
				removeClassName(rowElement,"rowHighlight");
				rowElement.value = "0";
			}
			else {
				appendClassName(rowElement,"rowHighlight");
				rowElement.value = "1";
			}
		}
	}
	
	// dealing with highlighting changes on radio or checkbox type Selectable Tables.  
	
	
	else {
		// if the rowselector is checked, highlight the row 
		if (ourRowSelectorFormElement.checked) {
			// dealing with checkboxes and hidden form fields. 
			if ((ourRowSelectorFormElement.getAttribute("type") == "checkbox") || (ourRowSelectorFormElement.getAttribute("type") == "hidden"))  {	
				appendClassName(rowElement,"rowHighlight");	
			}
			// dealing with radio buttons
			else {
				var inputNodeCollection = tableNode.getElementsByTagName("INPUT");
				for (var j=0;j<inputNodeCollection.length;j++) {

					if (inputNodeCollection[j].getAttribute("type")=="radio") {
						var tableRowNode = inputNodeCollection[j].parentNode.parentNode;

						if (!inputNodeCollection[j].checked)  {
							
							removeClassName(tableRowNode,"rowHighlight");	
						}
						else {
							appendClassName(tableRowNode,"rowHighlight");	
						}
					}
				}
			}
		}
		else {
			removeClassName(rowElement,"rowHighlight");
		}
	}
}


// this is only called once from init, and doesnt need to run again.  
TableManager.prototype.resizeTableColumns = function(tableId) {
	var tableDummyRow					= document.getElementById(tableId+"_dummyRow");
	var visualHeaderDivParent	= document.getElementById(tableId+"_headerDiv");
	var visualHeaderDiv				= visualHeaderDivParent.childNodes[0];
	var table								= document.getElementById(tableId);
	var tableScrollingDiv		= document.getElementById(tableId+"_scrollingTableDiv");

	var maxScreenDivHeight = 0;

	// this line is to workaround a mozilla bug with headerDiv height. This is set back to hidden after the resizing is completed
	visualHeaderDivParent.style.overflow = "scroll";

	// the purpose of this loop is to gently force various elements to wrap a little before table column sizes are reconciled with header sizes.
	for (var i=0; i<tableDummyRow.childNodes.length; i++) {
		// skip over text/whitespace nodes
		if (tableDummyRow.childNodes[i].nodeType==1)  {
			
			// forcing the table's cells to wrap a bit if they can.  Although many of these assignments will be refused by the table, they do have a good effect on cells that are very text-heavy. 
			if (document.all) {	
				tableDummyRow.childNodes[i].style.width = Math.floor(tableDummyRow.childNodes[i].offsetWidth*0.8);
			}

			// Conversely, forcing the header divs to wrap a bit. The way this is done is by crunching the outer div, which will cause some text in the inner div to possibly wrap (although it might be clipped by the outerdiv), and then having tricked the innerDiv into wrapping, set the outerdiv to the innerDiv's new offsetWidth, thus making the wrapping permanent.  particularly useful for mozilla
			visualHeaderDiv.childNodes[i].style.width =  Math.floor(visualHeaderDiv.childNodes[i].offsetWidth*0.8);;
			visualHeaderDiv.childNodes[i].style.width = visualHeaderDiv.childNodes[i].childNodes[0].offsetWidth;

			// keep track of the highest header. All heights will be set to this number after widths are dealt with.
			maxScreenDivHeight   = Math.max(maxScreenDivHeight,visualHeaderDiv.childNodes[i].offsetHeight);
		}
	}
	// the purpose of this loop is to normalize the widths of table columns with their respective header divs. 
	for (var i=0; i<tableDummyRow.childNodes.length; i++) {
		var tableColumnWidth  = tableDummyRow.childNodes[i].offsetWidth;
		var visualHeaderDivWidth   = visualHeaderDiv.childNodes[i].offsetWidth ;

		// skip over text/whitespace nodes
		if (tableDummyRow.childNodes[i].nodeType==1)  {

			// the case where the visual Header is wider than the table cells
			if (visualHeaderDivWidth > tableColumnWidth) {
				
				// we need to bump up the whole table's width property each time a column is changed. This will always increase the size of the table, never decrease it. background: if this is not done until the end, then some of the columns along the way would have refused to accept the larger widths. Although IE might do ok if this is omitted, mozilla does need it.
				var widthIncrement = visualHeaderDivWidth - tableColumnWidth;
				table.style.width = table.offsetWidth + widthIncrement; 
				
				// set the width of the div's within the column header cells.  background: if you try the straightforward approach of setting the width of the cells, the tableElement's constant desire to balance things against cell contents will occasionally rebuff you. This way, since you're making assignments to cell contents, the table has to listen. 
				tableDummyRow.childNodes[i].childNodes[0].style.width =   visualHeaderDivWidth - 10;				
			}
		}
		// again, skip over text/whitespace nodes. 
		if (visualHeaderDiv.childNodes[i].nodeType==1) {
			// the other side
			if (visualHeaderDivWidth < tableColumnWidth) {	
				visualHeaderDiv.childNodes[i].style.width = tableDummyRow.childNodes[i].offsetWidth;
			}
		}
	}
	// this loop is just to set the heights of the headerDivs so the borders extend down to the table edge.  This cannot be moved into the previous loop.
	for (var i=0; i<tableDummyRow.childNodes.length; i++) {
		if (visualHeaderDiv.childNodes[i].nodeType==1) {
			visualHeaderDiv.childNodes[i].style.height = maxScreenDivHeight +"px";
		}
	}
	

	// Need to set the visualHeaderDiv's width large enough so that the header divs can happily float next to eachother in a horizontal line. doesnt really matter how wide we make the visualHeaderDiv, since it's cropped behind the outer div anyway by virtue of the outerdiv's overflow:hidden. 
	visualHeaderDiv.style.width = table.offsetWidth+ 300;
	visualHeaderDiv.style.height= maxScreenDivHeight;
	visualHeaderDivParent.style.overflow = "hidden";
}


// this function is triggered by onscroll on the scrolling div.  the onscroll event handler is attached during init
TableManager.prototype.slideColumns = function(tableId) {
	var visualHeaderDivParent	= document.getElementById(tableId+"_headerDiv");
	var slidingColumnHeaders = visualHeaderDivParent.childNodes[0];
	var scrollingDiv	= document.getElementById(tableId+"_scrollingTableDiv");
	slidingColumnHeaders.style.left = -scrollingDiv.scrollLeft;
	var tableScrollingDiv		= document.getElementById(tableId+"_scrollingTableDiv");
	visualHeaderDivParent.style.width = tableScrollingDiv.offsetWidth;
}


function tableManager_windowResize() {
	ourTableManager.windowResize();
}




/* Taken from buttonManager.js */



/* TABS */
function TabManager() {

}

TabManager.prototype.init = function() {
   var divs = document.getElementsByTagName("DIV");
   for (var i=0;i<divs.length;i++) {
      if (( divs[i].className=="tabOn")|| ( divs[i].className=="tabOff"))  {
         // passes clicks from the div's themselves down into the link inside. This makes the tabs behave in a manner more consistent with application tabs. 
         if ( divs[i].className=="tabOff") {
            // a note on this click() method.   If you should find, on some new release of Mozilla, that anchorElements or other elements dont seem to have a click method or are throwing errors, then this might mean that a certain patch for mozilla's DOM has stopped working. (The patch in question concerns the fact that Mozilla does not currently give link elements a click method and that one must be then created for it.)
            divs[i].onclick = function() {this.childNodes[0].click();};
         }

         // min-width and min-height css doesnt work on ie6, so this goes in and does it by hand. 
         // note that there is currently an assumption that the second level tab divs are the IMMEDIATE children of the "secondaryTabSet" div. 
         if (document.all) {
            
            if (divs[i].parentNode.className=="secondaryTabSet") {
               if (divs[i].className == "tabOn") {
                  if (divs[i].offsetWidth < 83) divs[i].style.width = "85px";
               }
               else if (divs[i].className == "tabOff") {
                    if (divs[i].offsetWidth < 85) divs[i].style.width = "83px";
               }
            }
            else {
               if (divs[i].className == "tabOn") {
                  if (divs[i].offsetWidth < 111) divs[i].style.width = "115px";
               }
               else if (divs[i].className == "tabOff") {
                   if (divs[i].offsetWidth < 113) divs[i].style.width = "113px";
               }
            }
         }
      }
   }
}


function ButtonManager() {

}


ButtonManager.prototype.getWrapperReference = function(btnWrapperObj) {
   var   ob = btnWrapperObj;
   var className = ob.className;
   while (className.indexOf("bWrapper")==-1) {
      ob = ob.parentNode;
      className = ob.className;
   }
   return ob;
}

ButtonManager.prototype.hpButtonOver = function(btnWrapperObj) {
   if (!this.getButtonChild(btnWrapperObj).disabled) {
      var ob = this.getWrapperReference(btnWrapperObj);
      if (ob.className.indexOf("bEmphasized")!=-1) {
         ob.className = "bWrapperOver bEmphasized";
      }
      else {
         ob.className = "bWrapperOver";
      }
   }
}

ButtonManager.prototype.hpButtonUp = function(btnWrapperObj) {
   if (!this.getButtonChild(btnWrapperObj).disabled) {
      var ob = this.getWrapperReference(btnWrapperObj);
      if (ob.className.indexOf("bEmphasized")!=-1) {
         ob.className = "bWrapperUp bEmphasized";
      }
      else {
         ob.className = "bWrapperUp";
      }

   }
}

ButtonManager.prototype.hpButtonDown = function(btnWrapperObj) {
   var ob = this.getWrapperReference(btnWrapperObj);
   var childButton = this.getButtonChild(btnWrapperObj);
   if (!childButton.disabled) {
      if (ob.className.indexOf("bEmphasized")!=-1) {
         ob.className = "bWrapperDown bEmphasized";
      }
      else {
         ob.className = "bWrapperDown";
      }
      childButton.focus();
   }
}


ButtonManager.prototype.getButtonChild = function(divObj) {
   var obj = divObj
   while ((obj.childNodes[0]!=null) && ((obj.tagName!="BUTTON") && (obj.tagName!="INPUT"))  &&  (obj.tagName!="A") && (obj.tagName!="SUBMIT") ) {
      obj = obj.childNodes[0];   
   }
   return obj;
}


ButtonManager.prototype.init  = function() {
   var divs = document.getElementsByTagName("DIV");
   var agt=navigator.userAgent.toLowerCase();
   var is_gecko = (agt.indexOf('gecko') != -1);

   for (var i=0;i<divs.length;i++) {
      
      if (divs[i].className.indexOf("bWrapper")!=-1) {
            
         // some button styles might have two nested divs, making a 2px border, or three.  therefore we have to be careful when we look down inside the divs looking for the buttons. 
         var button = this.getButtonChild(divs[i]);

         // looking up to see if we're in a vertical button set. 
         var lookingForVerticalButtonSet = divs[i];
         while ((lookingForVerticalButtonSet) && (lookingForVerticalButtonSet.className!="verticalButtonSet")) {
            lookingForVerticalButtonSet = lookingForVerticalButtonSet.parentNode;
         }
         if (lookingForVerticalButtonSet) {
            button.style.width = "100%";
         }
         else if (!button.getAttribute("disableminimumwidth")){
            
            
            // setting widths based on which size button this is to be. 
            // tempting to use CSS property min-width and  accept that it only works on mozilla. However mozilla forces align left in this case, which does more harm than good to the design. 
            if (button.className.indexOf("hpButtonSmall")!=-1) {  

               if (!is_gecko && button.offsetWidth < 47 && button.offsetWidth > 20) button.style.width = "24px";
               if (!is_gecko && button.offsetWidth < 20) button.style.width = "16px";
               
            }
            else if (button.className.indexOf("hpButton")!=-1) {  

                button.style.width = "95px";
              
            }
            else if (button.className.indexOf("hpStdButtonLarge")!=-1) {  
                     button.style.width = "186px";              
            }
            else if (button.className.indexOf("hpStdButton")!=-1) {  
                     button.style.width = "86px";    
                               
            }
            
            else if (!is_gecko && button.className.indexOf("hpButtonVerySmall")!=-1) {
               // these are tiny buttons with no minimum width, like the help button on the pagetitle bar. 
            }

            // catchall 
            //  This line causes some mozilla problems with the vertical buttons btw, although it is no longer used by them. 
            else if (!is_gecko && button.offsetWidth < 83) button.style.width = "83px";
            
         }

         if (!button.disabled) {
            divs[i].onmousedown= function() {ourButtonManager.hpButtonDown(this);}
            divs[i].onmouseup= function() {ourButtonManager.hpButtonOver(this);}
            divs[i].onmouseover= function() {ourButtonManager.hpButtonOver(this);}
            divs[i].onmouseout= function() {ourButtonManager.hpButtonUp(this);}
            


            // this effectively transfers the onclick up to the level of the div. 
            // this makes the entire div+button structure effectively into the 'button', and moreover covers cases when the user clicks only one of the the right or bottom borders. 
            if (button.onclick) {   
               eval("divs[i].onclick = function() {var hardCodedFunction = "+button.onclick+" ;return hardCodedFunction();};");
               button.onclick = function(event) {};
            }
            

            var buttonChildren = divs[i].getElementsByTagName("BUTTON");
            var inputChildren = divs[i].getElementsByTagName("INPUT");
            for (var j=0;j<buttonChildren.length;j++) {

               buttonChildren[j].onfocus = function() {ourButtonManager.hpButtonDown(this);}
               buttonChildren[j].onblur = function() {ourButtonManager.hpButtonUp(this);}
            }
            for (var j=0;j<inputChildren.length;j++) {
               inputChildren[j].onfocus = function() {ourButtonManager.hpButtonDown(this);}
               inputChildren[j].onblur = function() {ourButtonManager.hpButtonUp(this);}
            }
         }
         else {
            if (divs[i].className.indexOf("bEmphasized")!=-1) {
               divs[i].className = "bWrapperDisabled bEmphasized";
            }
            else {
               divs[i].className = "bWrapperDisabled";
            }
         }
      }
   }
}


function onlyNumber(e)
{
  var num;
  var str;
  var match;
  
  // to support different browser
  if(window.event)
  {
    num = e.keyCode;
  }
  else if(e.which)
  {
     num = e.which;
  }
  
  str = String.fromCharCode(num);
  match = /[a-zA-Z`~!@#$%^&\*\(\)_\-\+=\|\\\}\{\]\["':;\?\/>\.<,]/;
  return !match.test(str);
}

function onlyNumberObj(obj)
{
  var x=obj.value;
  var match = /[a-zA-Z`~!@#$%^&\*\(\)_\-\+=\|\\\}\{\]\["':;\?\/>\.<,]/;

  if(match.test(x))
  {
    obj.value="";
  }
}


function onlyValidFaxNumber(e)
{
  var num;
  var str;
  var match;
  
  if(window.event)
  {
    num = e.keyCode;
  }
  else if(e.which)
  {
     num = e.which;
  }
  
  str = String.fromCharCode(num);
  match = /[a-zABCDEFGHIJKLMNOPQSTUVXYZ`~!@\$%\^&_=\|\\\{\}\[\]"':;\?\/\.,<>]/;
  return !match.test(str);
}
function onlyValidFaxHeaderNumber(e)
{
  var num;
  var str;
  var match;
  
  if(window.event)
  {
    num = e.keyCode;
  }
  else if(e.which)
  {
     num = e.which;
  }
  
  str = String.fromCharCode(num);
  match = /[a-zABCDEFGHIJKLMNOPQRSTUVWXYZ`~!@\$%\^&\*_=\|\\\{\}\[\]"':;\?\/\.,<>]/;
  return !match.test(str);
}

function onlyValidFaxNumberObj(obj)
{
  var x=obj.value;
  var match = /[a-zABCDEFGHIJKLMNOPQSTUVXYZ`~!@\$%\^&_=\|\\\{\}\[\]"':;\?\/\.,<>]/;

  if(match.test(x))
  {
    obj.value="";
  }
}
function onlyValidFaxHeaderNumberObj(obj)
{
  var x=obj.value;
  var match = /[a-zABCDEFGHIJKLMNOPQRSTUVWXYZ`~!@\$%\^&\*_=\|\\\{\}\[\]"':;\?\/\.,<>]/;

  if(match.test(x))
  {
    obj.value="";
  }
}

function onlyValidName(e)
{
  var num;
  var str;
  var match;
  
  if(window.event)
  {
    num = e.keyCode;
  }
  else if(e.which)
  {
     num = e.which;
  }
  
  str = String.fromCharCode(num);
  // anything else of following symbols will be accepted
  match = /`|\$|\^|\||\{|\}|\[|\]|"|<|>|\?/;
  return !match.test(str);
}

function onlyValidNameObj(obj)
{
  var x=obj.value;
  // anything else of following symbols will be accepted
  var match = /`|\$|\^|\||\{|\}|\[|\]|"|<|>|\?/;

  if(match.test(x))
  {
    obj.value="";
  }
}

function clrWebAdminPwdField()
{
  var form = document.SecurityForm;
  form.UserPwd.value="";
  form.PwdCheck.value="";
}


