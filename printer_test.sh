#!/bin/bash

# === Variables ===
PRINTER_INFO_URL="http://192.168.0.11/index.htm?cat=info&page=printerInfo"  # URL contenant les infos (status imprimante / cartouches et total pages imprimées)
REPORT_PRINT_URL="http://192.168.0.11/reportPrint.htm?ReportID=ConfigPage"  # URL pour lancer le rapport d'auto-test
PAGE_PATH="/volume1/web/cronPrinterPage.html"   # chemin de sauvegarde de la page d'info
LOG_FILE="/volume1/web/cronPrinterTest.log"     # chemin du fichier de log

# echo "mailcontent" | mail -s testSubject myemail@mail.com

# === Debut du script ===
# Suppression de la page d'info récupérer lors de l'éxécution précédente
echo -e "\nSuppression de l'ancienne page d'info\n"
rm -f $PAGE_PATH

# Téléchargement de la page d'info de l'imprimante
echo -ne "Téléchargement de la page d'info : "
wget -t 10 -T 5 -kO "$PAGE_PATH" "$PRINTER_INFO_URL" &> "$LOG_FILE"

# Si la page enregistré a un poids égale à zéro
if [ ! -s "$PAGE_PATH" ];then
    echo -e "- La page d'info semble injoignable"
    # Vérification de la dernière date d'impression
    # Si page imprimée, il y'a plus de 7 jours, envoyer alerte
    # Si page imprimée, il y'a plus de 10 jours, envoyer grosse alerte et faire bipper le nas

# Sinon
else
    echo -e "- Page d'info récupérée"
    
    # Parsage de la page pour récupéré les status (normal, warning, critical) et le nombre de pages imprimées
    PRINTER_STATUS=$(  awk '/icon_status_/ {split($2, a, "icon_status_|.gif"); print a[3]}' $PAGE_PATH | sed -n '3p')
    BLACK_INK_STATUS=$(awk '/icon_status_/ {split($2, a, "icon_status_|.gif"); print a[3]}' $PAGE_PATH | sed -n '1p')
    COLOR_INK_STATUS=$(awk '/icon_status_/ {split($2, a, "icon_status_|.gif"); print a[3]}' $PAGE_PATH | sed -n '2p')
    TOTAL_PRINTED_PAGES=$(awk '/Total/ {getline; getline; getline; print $1}' $PAGE_PATH )

    # Récupération avec sed (pour info, à laisser commenté)
    # PRINTER_STATUS=$(  sed -rn 's#/icon_status_([^.]+).*#\1#p' $PAGE_PATH | sed -n '3p')
    # BLACK_INK_STATUS=$(sed -rn 's#/icon_status_([^.]+).*#\1#p' $PAGE_PATH | sed -n '1p')
    # COLOR_INK_STATUS=$(sed -rn 's#/icon_status_([^.]+).*#\1#p' $PAGE_PATH | sed -n '2p')
    # TOTAL_PRINTED_PAGES=$(sed -rn '/Page Count/{n;n;n;s/ *//p}' $PAGE_PATH)
    
    echo -ne "\nEtat de l'imprimante:            "
    case $PRINTER_STATUS in
        normal) 
            PRINTER_STATUS=1
            echo "Ok"
            ;;
        warning) 
            PRINTER_STATUS=2
            echo "Attention !"
            # Envoi alerte
            ;;
        critical) 
            PRINTER_STATUS=3
            echo "Critique !!!"
            # Envoi grosse alerte
            ;;
        *)  echo "Valeur inattendue !!!"
            # Envoi grosse alerte
            break
            ;;
    esac

    echo -ne "Etat de la cartouche noire:      "
    case $BLACK_INK_STATUS in
        normal) 
            BLACK_INK_STATUS=1
            echo "Ok"
            ;;
        warning) 
            BLACK_INK_STATUS=2
            echo "Attention !"
            # Envoi alerte
            ;;
        critical) 
            BLACK_INK_STATUS=3
            echo "Critique !!!"
            # Envoi grosse alerte
            ;;
        *)  echo "Valeur inattendue !!!"
            # Envoi grosse alerte
            break
            ;;
    esac

    echo -ne "Etat de la cartouche couleur:    "
    case $COLOR_INK_STATUS in
        normal) 
            COLOR_INK_STATUS=1
            echo "Ok"
            ;;
        warning) 
            COLOR_INK_STATUS=2
            echo "Attention !"
            # Envoi alerte
            ;;
        critical) 
            COLOR_INK_STATUS=3
            echo "Critique !!!"
            # Envoi grosse alerte
            ;;
        *)  echo "Valeur inattendue !!!"
            # Envoi grosse alerte
            break
            ;;
    esac

    echo -e "Nombre total de pages imprimées: $TOTAL_PRINTED_PAGES"
    
    # SI $PRINTER_STATUS == ok & $BLACK_INK_STATUS == (normal|warning)
    if [ $PRINTER_STATUS == 1 ] && [ $BLACK_INK_STATUS -le 2 ] && [ $COLOR_INK_STATUS -le 2 ]; then
        echo -e "\n\nTout semble Ok pour lancer l'impression du rapport d'auto-test"
        # Téléchargement de la page d'info de l'imprimante
        echo -ne "Téléchargement de la page d'impression du rapport : "
        #wget -t 10 -T 5 -kO "$PAGE_PATH" "$REPORT_PRINT_URL" &> "$LOG_FILE"
    else
        echo -e "\n\nIl y'a possiblement un problème à vérifier sur l'imprimante"
    fi
fi


exit